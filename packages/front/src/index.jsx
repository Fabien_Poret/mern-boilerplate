import React, { useState }  from 'react'
import ReactDOM from 'react-dom'
import { HashRouter, Switch, Route, withRouter} from "react-router-dom"
import { ToastContainer, toast } from 'react-toastify'

import NavBar from './components/Navbar'
import PrivateRoute from './components/PrivateRoute'
import AuthContext from './contexts/AuthContext'
import Dashboard from './pages/Dashboard'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import { DashboardRoute, HomeRoute, LoginRoute, RegisterRoute } from './constants/routes'
import userAPI from './services/usersAPI'

import "react-toastify/dist/ReactToastify.css"

userAPI.setup()

function App() {
  const [ isAuthenticated, setIsAuthenticated ] = useState(userAPI.isAuthenticated())
  const NavBarWithRouter = withRouter(NavBar)

  return (
    <AuthContext.Provider value={{
      isAuthenticated,
      setIsAuthenticated
    }}>
      <HashRouter>
        <NavBarWithRouter/>
        <main className="container pt-5">
          <Switch>
            <Route path={LoginRoute} component={Login} />
            <Route path={RegisterRoute} component={Register} />
            <PrivateRoute path={DashboardRoute} component={Dashboard}/>
            <Route path={HomeRoute} component={Home}/>
          </Switch>
        </main>
      </HashRouter>
      <ToastContainer position={toast.POSITION.BOTTOM_LEFT}/>
    </AuthContext.Provider>
  )
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)
