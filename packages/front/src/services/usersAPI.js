import axios from 'axios'
import jwtDecode from "jwt-decode"

import { USER_API } from './config'

function isAuthenticated(){
    const token = window.localStorage.getItem("authToken")
    if(token){
        const { exp: expiration } = jwtDecode(token)
        if(expiration *1000 > new Date().getTime())
        {
            return true
        } 
        return false
    }
    return false
}

function login(credentials){
    return axios.post(`${USER_API}/login`, credentials)
    .then(response => response.data.token)
    .then(token => {
        window.localStorage.setItem("authToken", token)
        axios.defaults.headers["Authorization"] = 'Bearer ' + token
    })
}

function logout(){
    window.localStorage.removeItem("authToken")
    delete axios.defaults.headers["Authorization"]
}

function register(credentials){
    return axios.post(`${USER_API}/register`, credentials).then(response => console.log(response))
}

function setAxiosToken(token) {
    axios.defaults.headers["Authorization"] = 'Bearer ' + token;
}

function setup(){
    const token = window.localStorage.getItem("authToken")
    if(token){
        const { exp: expiration } = jwtDecode(token)
        if(expiration *1000 > new Date().getTime())
        {
            setAxiosToken(token)
        } 
    }
}

export default {
    isAuthenticated,
    logout,
    login,
    register,
    setup
}