// TODO: ERROR RETURN BY API 

import React, { useContext, useState } from 'react'
import { DashboardRoute } from '../constants/routes'
import AuthContext from '../contexts/AuthContext'
import usersAPI from '../services/usersAPI'

import Field from '../components/Field'
import ThreeDots from '../components/Loaders/ThreeDots'
import { toast } from 'react-toastify'

function Login({history}) {
    const [ disabled, setDisabled ] = useState(true)
    const { setIsAuthenticated } = useContext(AuthContext)
    const [ loading, setLoading ] = useState(false)
    const [ error, setError ] = useState()
    const [ credentials, setCredentials ] = useState({
        email: "",
        password: ""
    })

    const handleChange = (event) => {
        const value = event.currentTarget.value
        const name = event.currentTarget.name
        setCredentials({...credentials, [name]: value})
        if(
            credentials.email.length > 1 &&
            credentials.password.length > 1
        ) {
            setDisabled(false)
        } else {
            setDisabled(true)
        }
    }
    
    const handleSubmit = async event => {
        event.preventDefault()
        try{
            setLoading(true)
            await usersAPI.login(credentials)
            setIsAuthenticated(true)
            toast.success('Login OK')
            history.replace(DashboardRoute)
        }catch(error){
            console.log(error)
            setLoading(false)
            setError("An error")
        }
    }

    return(
        <>
            <h2>Login</h2>
            {!loading 
                ? 
                    <form onSubmit={handleSubmit}>
                        <Field 
                            label="Email" 
                            name="email" 
                            value={credentials.email} 
                            onChange={handleChange} 
                            placeholder="Email" 
                            error={error}
                        />
                        <Field 
                            label="password" 
                            name="password" 
                            type="password"
                            value={credentials.password} 
                            onChange={handleChange} 
                            placeholder="Password" 
                            error={error}
                        />
                        <div className="form-group mt-3">
                            <button type="submit" className="btn btn-success"
                                disabled={disabled}
                            >
                                Submit
                            </button>
                        </div>
                    </form>
                : 
                    <ThreeDots />
            }
        </>           
    )
}

export default Login