// TODO: ERROR RETURN BY API 

import React, { useState } from 'react'
import { LoginRoute } from '../constants/routes'
import usersAPI from '../services/usersAPI'

import Field from '../components/Field'
import ThreeDots from '../components/Loaders/ThreeDots'
import { toast } from 'react-toastify'

function Register({history}) {
    const [ disabled, setDisabled ] = useState(true)
    const [ loading, setLoading ] = useState(false)
    const [ error, setError ] = useState()
    const [ credentials, setCredentials ] = useState({
        email: "",
        name: "",
        password: "",
        password2: ""
    })

    const handleChange = (event) => {
        const value = event.currentTarget.value
        const name = event.currentTarget.name
        setCredentials({...credentials, [name]: value})
        if(
            credentials.email.length > 1 &&
            credentials.name.length > 1 &&
            credentials.password.length > 1 &&
            credentials.password2.length > 1 
        ) {
            setDisabled(false)
        } else {
            setDisabled(true)
        }
    }

    const handleSubmit = async event => {
        event.preventDefault()
        if(credentials.password === credentials.password2) {
            try{
                setLoading(true)
                await usersAPI.register(credentials)
                toast.success('Register OK')
                history.replace(LoginRoute)
            }catch(error){
                console.log(error)
                setLoading(false)
                setError("error")
            }
        }
    }

    return(
        <>
            <h2>Register</h2>
            {!loading 
                ? 
                    <form onSubmit={handleSubmit}>
                        <Field 
                            label="Name" 
                            name="name" 
                            value={credentials.name} 
                            onChange={handleChange} 
                            placeholder="Name" 
                            error={error}
                        />
                        <Field 
                            label="Email" 
                            name="email" 
                            value={credentials.email} 
                            onChange={handleChange} 
                            placeholder="Email" 
                            error={error}
                        />
                        <Field 
                            label="password" 
                            name="password" 
                            type="password"
                            value={credentials.password} 
                            onChange={handleChange} 
                            placeholder="Password" 
                            error={error}
                        />
                        <Field 
                            label="password2" 
                            name="password2" 
                            type="password"
                            value={credentials.password2} 
                            onChange={handleChange} 
                            placeholder="password2" 
                            error={error}
                        />
                        <div className="form-group mt-3">
                            <button type="submit" className="btn btn-success"
                                disabled={disabled}
                            >
                                Submit
                            </button>
                        </div>
                    </form>
                : 
                    <ThreeDots />
            }
        </>           
    )
}

export default Register