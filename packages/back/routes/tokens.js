const permissions = require("commons/src/constants/permissions");
const express = require("express");
const authWithJWT = require("../middlewares/authWithJWT");
const router = express.Router();

router.get('/', authWithJWT, (req, res) => {
    console.log(req)
    if(req.userData.role === permissions.ADMIN){
        res.json('Can use this route, you are admin!');
    }
});

module.exports = router;