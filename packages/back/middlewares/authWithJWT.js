const jwt = require("jsonwebtoken");
const keys = require("../config/keys")

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, keys.secretOrKey);
    req.userData = { email: decodedToken.email, name: decodedToken.name, id: decodedToken.id, role: decodedToken.role };
    next();
  } catch (error) {
    res.status(401).json({ message: "Auth failed!" });
  }
};